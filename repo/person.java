public class Person {
	private String name;
	private String surname;
	private String phoneNumber;
	private User us;
	private Collection<Address> address;
	private Collection<Role> role;
	
	void addAddress(Address){
		address.add(adres);
	}

	void removeAddress(Address){
		address.remove(adres);
	}

	void addRole(Role){
		role.add(rola);
	}

	void removeRole(Role){
		role.remove(rola);
	}

	public Person(String imie, String nazwisko, String phone){
		this.name = imie;
		this.surname = nazwisko;
		this.phoneNumber = phone;
	}

	public void setUser(User user){
		if (user != us){
			this.us = user;
			user.setUser(this);
		}
	}
	
	public void setName(String mName){
		this.name = mName;
	}

	public void setSurname(String mSurname){
		this.surname = mSurname;
	}

	public void setPhoneNumber(String mPhoneNumber){
		this.phoneNumber = mPhoneNumber;
	}

	public String getName(){
		return this.name;
	}
	
	public String getSurname(){
		return this.surname;
	}
	
	public String getPhoneNumber(){
		return this.phoneNumber;
	}

}
