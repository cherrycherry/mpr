public class Permission{
	private String permission;
	private Role role;

	public Permission(String mPermission){
		this.permission = mPermission;
	}

	public void setRole(Role role){
		if (this.role != null)
			this.role.removePermission(this);
		this.role = role;
		this.role.addPermission(this);
	}
	
	public void setPermission(String mPermission){
		this.permission = mPermission;
	}

	public String getPermission(){
		return this.permission;
	}
