public class Address {
	private String ulica;
	private String numerDomu;
	private String miejscowosc;
	private String kodPocztowy;
	private Person person;

	public Address(String mUlica, String mNumerDomu, String mMiejscowosc, String mKodPocztowy){
		this.ulica = mUlica;
		this.numerDomu = mNumerDomu;
		this.miejscowosc = mMiejscowosc;
		this.kodPocztowy = mKodPocztowy;
	}
	
	public void setPerson(Person person){
		if (this.person != null)
			this.person.removeAddress(this);
		this.person = person;
		this.person.addAddress(this);
	}

	public void setUlica(String street){
		this.ulica = street
	}

	public void setNumerDomu(String number){
		this.numerDomu = number;
	}

	public void setMiejscowosc(String town){
		this.miejscowosc = town;
	}

	public void setKodPocztowy(String postalCode){
		this.kodPocztowy = postalCode;
	}

	public String getUlica(){
		return this.ulica;
	}
	
	public String getNumerDomu(){
		return this.numerDomu;
	}

	public String getMiejscowosc(){
		return this.miejscowosc;
	}
	
	public String getKodPocztowy(){
		return this.kodPocztowy;
	}
