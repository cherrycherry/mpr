public class User {
	private String login;
	private String password;

	private Person osoba;

	public User(String l, String pwd){
		this.login = l;
		this.password = pwd;
	}

	public void setPerson(Person person) {
		if (person != osoba){
			this.osoba = person;
			person.setUser(this);
		}

	}
	
	public void setLogin(String mLogin){
		this.login = mLogin;
	}

	public void setPassword(String mPassword){
		this.password = mPassword;
	}
	
	public String getLogin(){
		return this.login;
	}
	
	public String getPassword(){
		return this.password;
	}
} 
